SCANNER := wayland-scanner

PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/share/man
BASHCOMPDIR=$(PREFIX)/share/bash-completion/completions
ZSHCOMPDIR=$(PREFIX)/share/zsh/vendor-completions

CFLAGS=-Wall -Werror -Wextra -Wpedantic -Wno-unused-parameter -Wconversion $\
	-Wformat-security -Wformat -Wsign-conversion -Wfloat-conversion $\
	-Wunused-result $(shell pkg-config --cflags pixman-1)
LIBS=-lwayland-client $(shell pkg-config --libs pixman-1) -lrt
OBJ=wayneko.o wlr-layer-shell-unstable-v1.o xdg-shell.o ext-idle-notify-v1.o
GEN=wlr-layer-shell-unstable-v1.c wlr-layer-shell-unstable-v1.h $\
	xdg-shell.c xdg-shell.h $\
	ext-idle-notify-v1.c ext-idle-notify-v1.h

wayneko: $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $(OBJ) $(LIBS)

$(OBJ): $(GEN)

%.c: %.xml
	$(SCANNER) private-code < $< > $@

%.h: %.xml
	$(SCANNER) client-header < $< > $@

install: wayneko
	install        -D wayneko   $(DESTDIR)$(BINDIR)/wayneko
	install -m 644 -D wayneko.1 $(DESTDIR)$(MANDIR)/man1/wayneko.1
	install        -D bash-completion $(DESTDIR)$(BASHCOMPDIR)/wayneko
	install        -D zsh-completion $(DESTDIR)$(ZSHCOMPDIR)/_wayneko

uninstall:
	$(RM) $(DESTDIR)$(BINDIR)/wayneko
	$(RM) $(DESTDIR)$(MANDIR)/man1/wayneko.1
	$(RM) $(DESTDIR)$(BASHCOMPDIR)/wayneko
	$(RM) $(DESTDIR)$(ZSHCOMPDIR)/_wayneko

clean:
	$(RM) wayneko $(GEN) $(OBJ)

.PHONY: clean install
