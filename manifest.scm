(use-modules
  (gnu packages freedesktop)
  (gnu packages pkg-config)
  (gnu packages xdisorg)
  (guix packages)
  (guix gexp)
  (guix utils)
  (guix build-system gnu)
  ((guix licenses) #:prefix license:))

;; https://codeberg.org/anemofilia/radix/src/branch/main/modules/radix/packages/toys.scm
(packages->manifest
  (list (package
          (name "wayneko")
          (version "dev")
          (source
            (local-file "." "guile-checkout"
                        #:recursive? #t))
          (build-system gnu-build-system)
          (arguments
            (list #:make-flags
                  #~(list (string-append "CC=" #$(cc-for-target))
                          (string-append "PREFIX=" #$output))
                  #:phases
                  #~(modify-phases %standard-phases
                                   (delete 'configure)
                                   (delete 'check))))
          (inputs
            (list wayland
                  pkg-config
                  pixman))
          (home-page "https://codeberg.org/look/wayneko")
          (synopsis "Neko on Wayland")
          (description "The cute oneko cat chasing your mouse pointer, but now on wayland.")
          (license license:gpl3))))
